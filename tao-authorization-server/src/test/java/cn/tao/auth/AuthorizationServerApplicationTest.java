package cn.tao.auth;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @Author tao
 * @Date 2024/8/18 10:31
 * @Description
 * @Version 1.0
 */
@SpringBootTest
public class AuthorizationServerApplicationTest {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Test
    public void test1(){
        redisTemplate.opsForValue().set("tao","sh");
        System.out.println(redisTemplate.opsForValue().get("tao"));
    }

}
