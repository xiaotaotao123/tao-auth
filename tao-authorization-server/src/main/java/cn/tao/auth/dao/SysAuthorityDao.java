package cn.tao.auth.dao;

import cn.tao.auth.dao.entity.SysAuthorityDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统菜单 Dao
 *
 * @author tao
 * @date 2024-08-19
 */
public interface SysAuthorityDao extends BaseMapper<SysAuthorityDO> {

}
