package cn.tao.auth.dao;

import cn.tao.auth.dao.entity.OAuth2ThirdAccountDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 三方登录账户信息表 Dao
 *
 * @author tao
 * @date 2024-08-19
 */
public interface OAuth2ThirdAccountDao extends BaseMapper<OAuth2ThirdAccountDO> {

}
