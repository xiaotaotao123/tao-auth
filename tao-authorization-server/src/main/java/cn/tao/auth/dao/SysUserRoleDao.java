package cn.tao.auth.dao;

import cn.tao.auth.dao.entity.SysUserRoleDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户角色表 Dao
 *
 * @author tao
 * @date 2024-08-19
 */
public interface SysUserRoleDao extends BaseMapper<SysUserRoleDO> {

}
