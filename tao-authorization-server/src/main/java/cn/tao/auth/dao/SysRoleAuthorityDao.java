package cn.tao.auth.dao;

import cn.tao.auth.dao.entity.SysRoleAuthorityDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色菜单 Dao
 *
 * @author tao
 * @date 2024-08-19
 */
public interface SysRoleAuthorityDao extends BaseMapper<SysRoleAuthorityDO> {

}
