package cn.tao.auth.dao.entity;

import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 系统菜单 Entity
 *
 * @author tao
 * @date 2024-08-19
 */
@Data
@TableName("sys_authority")
public class SysAuthorityDO {

    /**
     * 所需权限
     */
    @TableField("authority")
    private String authority;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 创建人
     */
    @TableField("create_user_id")
    private Integer createUserId;

    /**
     * 0:启用,1:删除
     */
    @TableField("deleted")
    private Boolean deleted;

    /**
     * 菜单自增ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Integer id;

    /**
     * 父菜单ID
     */
    @TableField("menu_pid")
    private Integer menuPid;

    /**
     * 菜单名称
     */
    @TableField("name")
    private String name;

    /**
     * 排序
     */
    @TableField("sort")
    private Byte sort;

    /**
     * 0:菜单,1:接口
     */
    @TableField("type")
    private Byte type;

    /**
     * 跳转URL
     */
    @TableField("url")
    private String url;

}
