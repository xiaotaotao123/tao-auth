package cn.tao.auth.dao.entity;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 角色菜单 Entity
 *
 * @author tao
 * @date 2024-08-19
 */
@Data
@TableName("sys_role_authority")
public class SysRoleAuthorityDO {

    /**
     * 权限菜单ID
     */
    @TableField("authority_id")
    private Integer authorityId;

    /**
     * 角色菜单关联表自增ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Integer id;

    /**
     * 角色ID
     */
    @TableField("role_id")
    private Integer roleId;

}
