package cn.tao.auth.dao.entity;

import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 三方登录账户信息表 Entity
 *
 * @author tao
 * @date 2024-08-19
 */
@Data
@TableName("oauth2_third_account")
public class OAuth2ThirdAccountDO {

    /**
     * 博客地址
     */
    @TableField("blog")
    private String blog;

    /**
     * 绑定时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Integer id;

    /**
     * 地址
     */
    @TableField("location")
    private String location;

    /**
     * 三方登录类型
     */
    @TableField("type")
    private String type;

    /**
     * 三方登录唯一id
     */
    @TableField("unique_id")
    private String uniqueId;

    /**
     * 修改时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 用户表主键
     */
    @TableField("user_id")
    private Integer userId;

}
