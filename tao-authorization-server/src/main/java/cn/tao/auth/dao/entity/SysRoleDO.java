package cn.tao.auth.dao.entity;

import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 角色 Entity
 *
 * @author tao
 * @date 2024-08-19
 */
@Data
@TableName("sys_role")
public class SysRoleDO {

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 创建人
     */
    @TableField("create_user_id")
    private Integer createUserId;

    /**
     * 0:启用,1:删除
     */
    @TableField("deleted")
    private Boolean deleted;

    /**
     * 角色自增ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Integer id;

    /**
     * 角色名
     */
    @TableField("role_name")
    private String roleName;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

}
