package cn.tao.auth.dao;

import cn.tao.auth.dao.entity.OAuth2BasicUserDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 基础用户信息表 Dao
 *
 * @author tao
 * @date 2024-08-19
 */
public interface OAuth2BasicUserDao extends BaseMapper<OAuth2BasicUserDO> {

}
