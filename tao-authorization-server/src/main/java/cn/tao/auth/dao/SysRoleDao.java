package cn.tao.auth.dao;

import cn.tao.auth.dao.entity.SysRoleDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色 Dao
 *
 * @author tao
 * @date 2024-08-19
 */
public interface SysRoleDao extends BaseMapper<SysRoleDO> {

}
