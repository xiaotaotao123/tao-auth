package cn.tao.auth.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 基础用户信息表 Entity
 *
 * @author tao
 * @date 2024-08-19
 */
@Data
@TableName("oauth2_basic_user")
public class OAuth2BasicUserDO {

    /**
     * 账号
     */
    @TableField("account")
    private String account;

    /**
     * 头像地址
     */
    @TableField("avatar_url")
    private String avatarUrl;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 是否已删除
     */
    @TableField("deleted")
    private Boolean deleted;

    /**
     * 邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Integer id;

    /**
     * 手机号
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 用户名、昵称
     */
    @TableField("name")
    private String name;

    /**
     * 密码
     */
    @TableField("password")
    private String password;

    /**
     * 用户来源
     */
    @TableField("source_from")
    private String sourceFrom;

    /**
     * 修改时间
     */
    @TableField("update_time")
    private Date updateTime;

}
