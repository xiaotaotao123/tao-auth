package cn.tao.auth.service.bo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 用户角色表 BO
 *
 * @author tao
 * @date 2024-08-19
 */
@Data
@Schema(description = "用户角色表BO")
public class SysUserRoleBO {

    /**
     * 角色ID
     */
    @Schema(description = "角色ID")
    private Integer roleId;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private Integer userId;

}
