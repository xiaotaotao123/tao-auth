package cn.tao.auth.service;

import cn.tao.auth.controller.query.SysRoleAuthorityQuery;
import cn.tao.auth.controller.vo.SysRoleAuthorityVO;
import cn.tao.auth.dao.entity.SysRoleAuthorityDO;
import cn.tao.auth.service.bo.SysRoleAuthorityBO;
import cn.tao.base.BizPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 角色菜单 Service接口
 *
 * @author tao
 * @date 2024-08-19
 */
public interface SysRoleAuthorityService extends IService<SysRoleAuthorityDO> {

    /**
     * 查询（分页）
     *
     * @param query     SysRoleAuthorityQuery
     * @return Page<SysRoleAuthorityVO>
     */
    BizPage<SysRoleAuthorityVO> findSysRoleAuthorityPage(SysRoleAuthorityQuery query);

    /**
     * 查询（所有）
     *
     * @param query SysRoleAuthorityQuery
     * @return List<SysRoleAuthorityVO>
     */
    List<SysRoleAuthorityVO> findSysRoleAuthorityList(SysRoleAuthorityQuery query);

    /**
     * 新增
     *
     * @param bo SysRoleAuthorityBO
     */
    void addSysRoleAuthority(SysRoleAuthorityBO bo);

    /**
     * 修改
     *
     * @param id id
     * @param bo SysRoleAuthorityBO
     */
    void updateSysRoleAuthority(Long id, SysRoleAuthorityBO bo);

    /**
     * 删除
     *
     * @param id id
     */
    void deleteSysRoleAuthorityById(Long id);

}
