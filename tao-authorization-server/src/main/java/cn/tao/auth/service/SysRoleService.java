package cn.tao.auth.service;

import cn.tao.auth.controller.query.SysRoleQuery;
import cn.tao.auth.controller.vo.SysRoleVO;
import cn.tao.auth.dao.entity.SysRoleDO;
import cn.tao.auth.service.bo.SysRoleBO;
import cn.tao.base.BizPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 角色 Service接口
 *
 * @author tao
 * @date 2024-08-19
 */
public interface SysRoleService extends IService<SysRoleDO> {

    /**
     * 查询（分页）
     *
     * @param query     SysRoleQuery
     * @return Page<SysRoleVO>
     */
    BizPage<SysRoleVO> findSysRolePage(SysRoleQuery query);

    /**
     * 查询（所有）
     *
     * @param query SysRoleQuery
     * @return List<SysRoleVO>
     */
    List<SysRoleVO> findSysRoleList(SysRoleQuery query);

    /**
     * 新增
     *
     * @param bo SysRoleBO
     */
    void addSysRole(SysRoleBO bo);

    /**
     * 修改
     *
     * @param id id
     * @param bo SysRoleBO
     */
    void updateSysRole(Long id, SysRoleBO bo);

    /**
     * 删除
     *
     * @param id id
     */
    void deleteSysRoleById(Long id);

}
