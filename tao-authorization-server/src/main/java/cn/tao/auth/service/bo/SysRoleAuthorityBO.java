package cn.tao.auth.service.bo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 角色菜单 BO
 *
 * @author tao
 * @date 2024-08-19
 */
@Data
@Schema(description = "角色菜单BO")
public class SysRoleAuthorityBO {

    /**
     * 权限菜单ID
     */
    @Schema(description = "权限菜单ID")
    private Integer authorityId;

    /**
     * 角色ID
     */
    @Schema(description = "角色ID")
    private Integer roleId;

}
