package cn.tao.auth.service.impl;

import cn.tao.auth.controller.query.SysRoleQuery;
import cn.tao.auth.controller.vo.SysRoleVO;
import cn.tao.auth.dao.SysRoleDao;
import cn.tao.auth.dao.entity.SysRoleDO;
import cn.tao.auth.mapper.SysRoleMapper;
import cn.tao.auth.service.SysRoleService;
import cn.tao.auth.service.bo.SysRoleBO;
import cn.tao.base.BizPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 角色 Service实现
 *
 * @author tao
 * @date 2024-08-19
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleDao, SysRoleDO> implements SysRoleService {

    @Resource
    private SysRoleMapper sysRoleMapper;

    private Wrapper<SysRoleDO> setQueryWrapper(SysRoleQuery query) {
        LambdaQueryWrapper<SysRoleDO> wrapper = Wrappers.lambdaQuery(SysRoleDO.class);
        // TODO 设置查询条件
        return wrapper;
    }

    @Override
    public BizPage<SysRoleVO> findSysRolePage(SysRoleQuery query) {
        Wrapper<SysRoleDO> queryWrapper = setQueryWrapper(query);
        IPage<SysRoleDO> page = this.page(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(query.getPageNum(), query.getPageSize()), queryWrapper);
        return new BizPage<>(page.getTotal(), sysRoleMapper.dosToVos(page.getRecords()));
    }

    @Override
    public List<SysRoleVO> findSysRoleList(SysRoleQuery query) {
        Wrapper<SysRoleDO> queryWrapper = setQueryWrapper(query);
        return sysRoleMapper.dosToVos(this.baseMapper.selectList(queryWrapper));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addSysRole(SysRoleBO bo) {
        this.save(sysRoleMapper.boToDo(bo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateSysRole(Long id, SysRoleBO bo) {
        this.saveOrUpdate(sysRoleMapper.boToDo(id, bo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteSysRoleById(Long id) {
        this.removeById(id);
    }

}
