package cn.tao.auth.service.impl;

import cn.tao.auth.controller.query.OAuth2ThirdAccountQuery;
import cn.tao.auth.controller.vo.OAuth2ThirdAccountVO;
import cn.tao.auth.dao.OAuth2ThirdAccountDao;
import cn.tao.auth.dao.entity.OAuth2ThirdAccountDO;
import cn.tao.auth.mapper.OAuth2ThirdAccountMapper;
import cn.tao.auth.service.OAuth2ThirdAccountService;
import cn.tao.auth.service.bo.OAuth2ThirdAccountBO;
import cn.tao.base.BizPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 三方登录账户信息表 Service实现
 *
 * @author tao
 * @date 2024-08-19
 */
@Service
public class OAuth2ThirdAccountServiceImpl extends ServiceImpl<OAuth2ThirdAccountDao, OAuth2ThirdAccountDO> implements OAuth2ThirdAccountService {

    @Resource
    private OAuth2ThirdAccountMapper oAuth2ThirdAccountMapper;

    private Wrapper<OAuth2ThirdAccountDO> setQueryWrapper(OAuth2ThirdAccountQuery query) {
        LambdaQueryWrapper<OAuth2ThirdAccountDO> wrapper = Wrappers.lambdaQuery(OAuth2ThirdAccountDO.class);
        // TODO 设置查询条件
        return wrapper;
    }

    @Override
    public BizPage<OAuth2ThirdAccountVO> findOAuth2ThirdAccountPage(OAuth2ThirdAccountQuery query) {
        Wrapper<OAuth2ThirdAccountDO> queryWrapper = setQueryWrapper(query);
        IPage<OAuth2ThirdAccountDO> page = this.page(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(query.getPageNum(), query.getPageSize()), queryWrapper);
        return new BizPage<>(page.getTotal(), oAuth2ThirdAccountMapper.dosToVos(page.getRecords()));
    }

    @Override
    public List<OAuth2ThirdAccountVO> findOAuth2ThirdAccountList(OAuth2ThirdAccountQuery query) {
        Wrapper<OAuth2ThirdAccountDO> queryWrapper = setQueryWrapper(query);
        return oAuth2ThirdAccountMapper.dosToVos(this.baseMapper.selectList(queryWrapper));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addOAuth2ThirdAccount(OAuth2ThirdAccountBO bo) {
        this.save(oAuth2ThirdAccountMapper.boToDo(bo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateOAuth2ThirdAccount(Long id, OAuth2ThirdAccountBO bo) {
        this.saveOrUpdate(oAuth2ThirdAccountMapper.boToDo(id, bo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteOAuth2ThirdAccountById(Long id) {
        this.removeById(id);
    }

}
