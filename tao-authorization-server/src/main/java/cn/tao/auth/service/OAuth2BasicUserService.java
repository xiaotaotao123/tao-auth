package cn.tao.auth.service;

import cn.tao.auth.controller.query.OAuth2BasicUserQuery;
import cn.tao.auth.controller.vo.OAuth2BasicUserVO;
import cn.tao.auth.dao.entity.OAuth2BasicUserDO;
import cn.tao.auth.service.bo.OAuth2BasicUserBO;
import cn.tao.base.BizPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 基础用户信息表 Service接口
 *
 * @author tao
 * @date 2024-08-19
 */
public interface OAuth2BasicUserService extends IService<OAuth2BasicUserDO> {

    /**
     * 查询（分页）
     *
     * @param query     OAuth2BasicUserQuery
     * @return Page<OAuth2BasicUserVO>
     */
    BizPage<OAuth2BasicUserVO> findOAuth2BasicUserPage(OAuth2BasicUserQuery query);

    /**
     * 查询（所有）
     *
     * @param query OAuth2BasicUserQuery
     * @return List<OAuth2BasicUserVO>
     */
    List<OAuth2BasicUserVO> findOAuth2BasicUserList(OAuth2BasicUserQuery query);

    /**
     * 新增
     *
     * @param bo OAuth2BasicUserBO
     */
    void addOAuth2BasicUser(OAuth2BasicUserBO bo);

    /**
     * 修改
     *
     * @param id id
     * @param bo OAuth2BasicUserBO
     */
    void updateOAuth2BasicUser(Long id, OAuth2BasicUserBO bo);

    /**
     * 删除
     *
     * @param id id
     */
    void deleteOAuth2BasicUserById(Long id);

}
