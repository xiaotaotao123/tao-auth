package cn.tao.auth.service;

import cn.tao.auth.controller.query.SysAuthorityQuery;
import cn.tao.auth.controller.vo.SysAuthorityVO;
import cn.tao.auth.dao.entity.SysAuthorityDO;
import cn.tao.auth.service.bo.SysAuthorityBO;
import cn.tao.base.BizPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 系统菜单 Service接口
 *
 * @author tao
 * @date 2024-08-19
 */
public interface SysAuthorityService extends IService<SysAuthorityDO> {

    /**
     * 查询（分页）
     *
     * @param query     SysAuthorityQuery
     * @return Page<SysAuthorityVO>
     */
    BizPage<SysAuthorityVO> findSysAuthorityPage(SysAuthorityQuery query);

    /**
     * 查询（所有）
     *
     * @param query SysAuthorityQuery
     * @return List<SysAuthorityVO>
     */
    List<SysAuthorityVO> findSysAuthorityList(SysAuthorityQuery query);

    /**
     * 新增
     *
     * @param bo SysAuthorityBO
     */
    void addSysAuthority(SysAuthorityBO bo);

    /**
     * 修改
     *
     * @param id id
     * @param bo SysAuthorityBO
     */
    void updateSysAuthority(Long id, SysAuthorityBO bo);

    /**
     * 删除
     *
     * @param id id
     */
    void deleteSysAuthorityById(Long id);

}
