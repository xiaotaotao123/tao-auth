package cn.tao.auth.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.tao.auth.controller.query.OAuth2BasicUserQuery;
import cn.tao.auth.controller.vo.OAuth2BasicUserVO;
import cn.tao.auth.dao.OAuth2BasicUserDao;
import cn.tao.auth.dao.SysAuthorityDao;
import cn.tao.auth.dao.SysRoleAuthorityDao;
import cn.tao.auth.dao.SysUserRoleDao;
import cn.tao.auth.dao.entity.OAuth2BasicUserDO;
import cn.tao.auth.dao.entity.SysAuthorityDO;
import cn.tao.auth.dao.entity.SysRoleAuthorityDO;
import cn.tao.auth.dao.entity.SysUserRoleDO;
import cn.tao.auth.mapper.OAuth2BasicUserMapper;
import cn.tao.auth.mapper.SysRoleAuthorityMapper;
import cn.tao.auth.mapper.SysUserRoleMapper;
import cn.tao.auth.service.OAuth2BasicUserService;
import cn.tao.auth.service.bo.OAuth2BasicUserBO;
import cn.tao.auth.service.bo.OAuth2BasicUserDetails;
import cn.tao.base.BizPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.annotation.Resource;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 基础用户信息表 Service实现
 *
 * @author tao
 * @date 2024-08-19
 */
@Service
public class OAuth2BasicUserServiceImpl extends ServiceImpl<OAuth2BasicUserDao, OAuth2BasicUserDO> implements OAuth2BasicUserService, UserDetailsService {

    @Resource
    private OAuth2BasicUserMapper oAuth2BasicUserMapper;
    @Resource
    private SysUserRoleDao sysUserRoleDao;
    @Resource
    private SysRoleAuthorityDao sysRoleAuthorityDao;
    @Resource
    private SysAuthorityDao sysAuthorityDao;

    private Wrapper<OAuth2BasicUserDO> setQueryWrapper(OAuth2BasicUserQuery query) {
        LambdaQueryWrapper<OAuth2BasicUserDO> wrapper = Wrappers.lambdaQuery(OAuth2BasicUserDO.class);
        // TODO 设置查询条件
        return wrapper;
    }

    @Override
    public BizPage<OAuth2BasicUserVO> findOAuth2BasicUserPage(OAuth2BasicUserQuery query) {
        Wrapper<OAuth2BasicUserDO> queryWrapper = setQueryWrapper(query);
        IPage<OAuth2BasicUserDO> page = this.page(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(query.getPageNum(), query.getPageSize()), queryWrapper);
        return new BizPage<>(page.getTotal(), oAuth2BasicUserMapper.dosToVos(page.getRecords()));
    }

    @Override
    public List<OAuth2BasicUserVO> findOAuth2BasicUserList(OAuth2BasicUserQuery query) {
        Wrapper<OAuth2BasicUserDO> queryWrapper = setQueryWrapper(query);
        return oAuth2BasicUserMapper.dosToVos(this.baseMapper.selectList(queryWrapper));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addOAuth2BasicUser(OAuth2BasicUserBO bo) {
        this.save(oAuth2BasicUserMapper.boToDo(bo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateOAuth2BasicUser(Long id, OAuth2BasicUserBO bo) {
        this.saveOrUpdate(oAuth2BasicUserMapper.boToDo(id, bo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteOAuth2BasicUserById(Long id) {
        this.removeById(id);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 在Security中“username”就代表了用户登录时输入的账号，在重写该方法时它可以代表以下内容：账号、手机号、邮箱、姓名等
        // “username”在数据库中不一定非要是一样的列，它可以是手机号、邮箱，也可以都是，最主要的目的就是根据输入的内容获取到对应的用户信息，如下方所示
        // 通过传入的账号信息查询对应的用户信息
        LambdaQueryWrapper<OAuth2BasicUserDO> wrapper = Wrappers.lambdaQuery(OAuth2BasicUserDO.class)
                .or(o -> o.eq(OAuth2BasicUserDO::getEmail, username))
                .or(o -> o.eq(OAuth2BasicUserDO::getMobile, username))
                .or(o -> o.eq(OAuth2BasicUserDO::getAccount, username));
        OAuth2BasicUserDO basicUser = baseMapper.selectOne(wrapper);
        if (basicUser == null) {
            throw new UsernameNotFoundException("账号不存在");
        }
        OAuth2BasicUserDetails oAuth2BasicUserDetails = oAuth2BasicUserMapper.dosToUserDetail(basicUser);
        // 通过用户角色关联表查询对应的角色
        List<SysUserRoleDO> userRoles = sysUserRoleDao.selectList(Wrappers.lambdaQuery(SysUserRoleDO.class).eq(SysUserRoleDO::getUserId, basicUser.getId()));
        List<Integer> rolesId = Optional.ofNullable(userRoles).orElse(Collections.emptyList()).stream().map(SysUserRoleDO::getRoleId).collect(Collectors.toList());
        if (CollUtil.isEmpty(rolesId)) {
            return oAuth2BasicUserDetails;
        }
        // 通过角色菜单关联表查出对应的菜单
        List<SysRoleAuthorityDO> roleMenus = sysRoleAuthorityDao.selectList(Wrappers.lambdaQuery(SysRoleAuthorityDO.class).in(SysRoleAuthorityDO::getRoleId, rolesId));
        List<Integer> menusId = Optional.ofNullable(roleMenus).orElse(Collections.emptyList()).stream().map(SysRoleAuthorityDO::getAuthorityId).collect(Collectors.toList());
        if (CollUtil.isEmpty(menusId)) {
            return oAuth2BasicUserDetails;
        }

        // 根据菜单ID查出菜单
        List<SysAuthorityDO> menus = sysAuthorityDao.selectBatchIds(menusId);
        Set<SimpleGrantedAuthority> authorities = Optional.ofNullable(menus).orElse(Collections.emptyList()).stream().map(SysAuthorityDO::getUrl).map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
        oAuth2BasicUserDetails.setAuthorities(authorities);
        return oAuth2BasicUserDetails;
    }
}
