package cn.tao.auth.service.impl;

import cn.tao.auth.controller.query.SysUserRoleQuery;
import cn.tao.auth.controller.vo.SysUserRoleVO;
import cn.tao.auth.dao.SysUserRoleDao;
import cn.tao.auth.dao.entity.SysUserRoleDO;
import cn.tao.auth.mapper.SysUserRoleMapper;
import cn.tao.auth.service.SysUserRoleService;
import cn.tao.auth.service.bo.SysUserRoleBO;
import cn.tao.base.BizPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 用户角色表 Service实现
 *
 * @author tao
 * @date 2024-08-19
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleDao, SysUserRoleDO> implements SysUserRoleService {

    @Resource
    private SysUserRoleMapper sysUserRoleMapper;

    private Wrapper<SysUserRoleDO> setQueryWrapper(SysUserRoleQuery query) {
        LambdaQueryWrapper<SysUserRoleDO> wrapper = Wrappers.lambdaQuery(SysUserRoleDO.class);
        // TODO 设置查询条件
        return wrapper;
    }

    @Override
    public BizPage<SysUserRoleVO> findSysUserRolePage(SysUserRoleQuery query) {
        Wrapper<SysUserRoleDO> queryWrapper = setQueryWrapper(query);
        IPage<SysUserRoleDO> page = this.page(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(query.getPageNum(), query.getPageSize()), queryWrapper);
        return new BizPage<>(page.getTotal(), sysUserRoleMapper.dosToVos(page.getRecords()));
    }

    @Override
    public List<SysUserRoleVO> findSysUserRoleList(SysUserRoleQuery query) {
        Wrapper<SysUserRoleDO> queryWrapper = setQueryWrapper(query);
        return sysUserRoleMapper.dosToVos(this.baseMapper.selectList(queryWrapper));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addSysUserRole(SysUserRoleBO bo) {
        this.save(sysUserRoleMapper.boToDo(bo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateSysUserRole(Long id, SysUserRoleBO bo) {
        this.saveOrUpdate(sysUserRoleMapper.boToDo(id, bo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteSysUserRoleById(Long id) {
        this.removeById(id);
    }

}
