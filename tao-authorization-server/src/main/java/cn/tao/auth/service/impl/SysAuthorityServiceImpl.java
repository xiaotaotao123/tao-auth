package cn.tao.auth.service.impl;

import cn.tao.auth.controller.query.SysAuthorityQuery;
import cn.tao.auth.controller.vo.SysAuthorityVO;
import cn.tao.auth.dao.SysAuthorityDao;
import cn.tao.auth.dao.entity.SysAuthorityDO;
import cn.tao.auth.mapper.SysAuthorityMapper;
import cn.tao.auth.service.SysAuthorityService;
import cn.tao.auth.service.bo.SysAuthorityBO;
import cn.tao.base.BizPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 系统菜单 Service实现
 *
 * @author tao
 * @date 2024-08-19
 */
@Service
public class SysAuthorityServiceImpl extends ServiceImpl<SysAuthorityDao, SysAuthorityDO> implements SysAuthorityService {

    @Resource
    private SysAuthorityMapper sysAuthorityMapper;

    private Wrapper<SysAuthorityDO> setQueryWrapper(SysAuthorityQuery query) {
        LambdaQueryWrapper<SysAuthorityDO> wrapper = Wrappers.lambdaQuery(SysAuthorityDO.class);
        // TODO 设置查询条件
        return wrapper;
    }

    @Override
    public BizPage<SysAuthorityVO> findSysAuthorityPage(SysAuthorityQuery query) {
        Wrapper<SysAuthorityDO> queryWrapper = setQueryWrapper(query);
        IPage<SysAuthorityDO> page = this.page(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(query.getPageNum(), query.getPageSize()), queryWrapper);
        return new BizPage<>(page.getTotal(), sysAuthorityMapper.dosToVos(page.getRecords()));
    }

    @Override
    public List<SysAuthorityVO> findSysAuthorityList(SysAuthorityQuery query) {
        Wrapper<SysAuthorityDO> queryWrapper = setQueryWrapper(query);
        return sysAuthorityMapper.dosToVos(this.baseMapper.selectList(queryWrapper));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addSysAuthority(SysAuthorityBO bo) {
        this.save(sysAuthorityMapper.boToDo(bo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateSysAuthority(Long id, SysAuthorityBO bo) {
        this.saveOrUpdate(sysAuthorityMapper.boToDo(id, bo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteSysAuthorityById(Long id) {
        this.removeById(id);
    }

}
