package cn.tao.auth.service;

import cn.tao.auth.controller.query.SysUserRoleQuery;
import cn.tao.auth.controller.vo.SysUserRoleVO;
import cn.tao.auth.dao.entity.SysUserRoleDO;
import cn.tao.auth.service.bo.SysUserRoleBO;
import cn.tao.base.BizPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 用户角色表 Service接口
 *
 * @author tao
 * @date 2024-08-19
 */
public interface SysUserRoleService extends IService<SysUserRoleDO> {

    /**
     * 查询（分页）
     *
     * @param query     SysUserRoleQuery
     * @return Page<SysUserRoleVO>
     */
    BizPage<SysUserRoleVO> findSysUserRolePage(SysUserRoleQuery query);

    /**
     * 查询（所有）
     *
     * @param query SysUserRoleQuery
     * @return List<SysUserRoleVO>
     */
    List<SysUserRoleVO> findSysUserRoleList(SysUserRoleQuery query);

    /**
     * 新增
     *
     * @param bo SysUserRoleBO
     */
    void addSysUserRole(SysUserRoleBO bo);

    /**
     * 修改
     *
     * @param id id
     * @param bo SysUserRoleBO
     */
    void updateSysUserRole(Long id, SysUserRoleBO bo);

    /**
     * 删除
     *
     * @param id id
     */
    void deleteSysUserRoleById(Long id);

}
