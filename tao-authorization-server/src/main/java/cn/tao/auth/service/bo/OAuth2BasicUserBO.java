package cn.tao.auth.service.bo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

/**
 * 基础用户信息表 BO
 *
 * @author tao
 * @date 2024-08-19
 */
@Data
@Schema(description = "基础用户信息表BO")
public class OAuth2BasicUserBO {

    /**
     * 账号
     */
    @Schema(description = "账号")
    private String account;

    /**
     * 头像地址
     */
    @Schema(description = "头像地址")
    private String avatarUrl;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Date createTime;


    /**
     * 是否已删除
     */
    @Schema(description = "是否已删除")
    private Boolean deleted;


    /**
     * 邮箱
     */
    @Schema(description = "邮箱")
    private String email;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String mobile;

    /**
     * 用户名、昵称
     */
    @Schema(description = "用户名、昵称")
    private String name;

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    /**
     * 用户来源
     */
    @Schema(description = "用户来源")
    private String sourceFrom;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间")
    private Date updateTime;


}
