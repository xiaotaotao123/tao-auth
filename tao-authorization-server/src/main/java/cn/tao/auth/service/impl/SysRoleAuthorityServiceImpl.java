package cn.tao.auth.service.impl;

import cn.tao.auth.controller.query.SysRoleAuthorityQuery;
import cn.tao.auth.controller.vo.SysRoleAuthorityVO;
import cn.tao.auth.dao.SysRoleAuthorityDao;
import cn.tao.auth.dao.entity.SysRoleAuthorityDO;
import cn.tao.auth.mapper.SysRoleAuthorityMapper;
import cn.tao.auth.service.SysRoleAuthorityService;
import cn.tao.auth.service.bo.SysRoleAuthorityBO;
import cn.tao.base.BizPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 角色菜单 Service实现
 *
 * @author tao
 * @date 2024-08-19
 */
@Service
public class SysRoleAuthorityServiceImpl extends ServiceImpl<SysRoleAuthorityDao, SysRoleAuthorityDO> implements SysRoleAuthorityService {

    @Resource
    private SysRoleAuthorityMapper sysRoleAuthorityMapper;

    private Wrapper<SysRoleAuthorityDO> setQueryWrapper(SysRoleAuthorityQuery query) {
        LambdaQueryWrapper<SysRoleAuthorityDO> wrapper = Wrappers.lambdaQuery(SysRoleAuthorityDO.class);
        // TODO 设置查询条件
        return wrapper;
    }

    @Override
    public BizPage<SysRoleAuthorityVO> findSysRoleAuthorityPage(SysRoleAuthorityQuery query) {
        Wrapper<SysRoleAuthorityDO> queryWrapper = setQueryWrapper(query);
        IPage<SysRoleAuthorityDO> page = this.page(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(query.getPageNum(), query.getPageSize()), queryWrapper);
        return new BizPage<>(page.getTotal(), sysRoleAuthorityMapper.dosToVos(page.getRecords()));
    }

    @Override
    public List<SysRoleAuthorityVO> findSysRoleAuthorityList(SysRoleAuthorityQuery query) {
        Wrapper<SysRoleAuthorityDO> queryWrapper = setQueryWrapper(query);
        return sysRoleAuthorityMapper.dosToVos(this.baseMapper.selectList(queryWrapper));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addSysRoleAuthority(SysRoleAuthorityBO bo) {
        this.save(sysRoleAuthorityMapper.boToDo(bo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateSysRoleAuthority(Long id, SysRoleAuthorityBO bo) {
        this.saveOrUpdate(sysRoleAuthorityMapper.boToDo(id, bo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteSysRoleAuthorityById(Long id) {
        this.removeById(id);
    }

}
