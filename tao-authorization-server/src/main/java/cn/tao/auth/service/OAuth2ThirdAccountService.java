package cn.tao.auth.service;

import cn.tao.auth.controller.query.OAuth2ThirdAccountQuery;
import cn.tao.auth.controller.vo.OAuth2ThirdAccountVO;
import cn.tao.auth.dao.entity.OAuth2ThirdAccountDO;
import cn.tao.auth.service.bo.OAuth2ThirdAccountBO;
import cn.tao.base.BizPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 三方登录账户信息表 Service接口
 *
 * @author tao
 * @date 2024-08-19
 */
public interface OAuth2ThirdAccountService extends IService<OAuth2ThirdAccountDO> {

    /**
     * 查询（分页）
     *
     * @param query     OAuth2ThirdAccountQuery
     * @return Page<OAuth2ThirdAccountVO>
     */
    BizPage<OAuth2ThirdAccountVO> findOAuth2ThirdAccountPage(OAuth2ThirdAccountQuery query);

    /**
     * 查询（所有）
     *
     * @param query OAuth2ThirdAccountQuery
     * @return List<OAuth2ThirdAccountVO>
     */
    List<OAuth2ThirdAccountVO> findOAuth2ThirdAccountList(OAuth2ThirdAccountQuery query);

    /**
     * 新增
     *
     * @param bo OAuth2ThirdAccountBO
     */
    void addOAuth2ThirdAccount(OAuth2ThirdAccountBO bo);

    /**
     * 修改
     *
     * @param id id
     * @param bo OAuth2ThirdAccountBO
     */
    void updateOAuth2ThirdAccount(Long id, OAuth2ThirdAccountBO bo);

    /**
     * 删除
     *
     * @param id id
     */
    void deleteOAuth2ThirdAccountById(Long id);

}
