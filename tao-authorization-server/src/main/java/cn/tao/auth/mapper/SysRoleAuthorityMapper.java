package cn.tao.auth.mapper;

import cn.tao.auth.dao.entity.SysRoleAuthorityDO;
import cn.tao.auth.service.bo.SysRoleAuthorityBO;
import cn.tao.auth.controller.vo.SysRoleAuthorityVO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * 角色菜单 mapper转换
 *
 * @author tao
 * @date 2024-08-19
 */
@Mapper(componentModel = "spring")
public interface SysRoleAuthorityMapper {

    /**
     * boToDo
     **/
    SysRoleAuthorityDO boToDo(SysRoleAuthorityBO bo);

    /**
     * boToDo
     **/
    SysRoleAuthorityDO boToDo(Long id, SysRoleAuthorityBO bo);

    /**
     * doToVo
     **/
    SysRoleAuthorityVO doToVo(SysRoleAuthorityDO entity);

    /**
     * dosToVos
     **/
    List<SysRoleAuthorityVO> dosToVos(List<SysRoleAuthorityDO> lists);

}
