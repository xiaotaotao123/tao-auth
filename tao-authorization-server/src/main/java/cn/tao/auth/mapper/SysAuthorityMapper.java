package cn.tao.auth.mapper;

import cn.tao.auth.dao.entity.SysAuthorityDO;
import cn.tao.auth.service.bo.SysAuthorityBO;
import cn.tao.auth.controller.vo.SysAuthorityVO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * 系统菜单 mapper转换
 *
 * @author tao
 * @date 2024-08-19
 */
@Mapper(componentModel = "spring")
public interface SysAuthorityMapper {

    /**
     * boToDo
     **/
    SysAuthorityDO boToDo(SysAuthorityBO bo);

    /**
     * boToDo
     **/
    SysAuthorityDO boToDo(Long id, SysAuthorityBO bo);

    /**
     * doToVo
     **/
    SysAuthorityVO doToVo(SysAuthorityDO entity);

    /**
     * dosToVos
     **/
    List<SysAuthorityVO> dosToVos(List<SysAuthorityDO> lists);

}
