package cn.tao.auth.mapper;

import cn.tao.auth.dao.entity.OAuth2BasicUserDO;
import cn.tao.auth.service.bo.OAuth2BasicUserBO;
import cn.tao.auth.controller.vo.OAuth2BasicUserVO;
import cn.tao.auth.service.bo.OAuth2BasicUserDetails;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * 基础用户信息表 mapper转换
 *
 * @author tao
 * @date 2024-08-19
 */
@Mapper(componentModel = "spring")
public interface OAuth2BasicUserMapper {

    /**
     * boToDo
     **/
    OAuth2BasicUserDO boToDo(OAuth2BasicUserBO bo);

    /**
     * boToDo
     **/
    OAuth2BasicUserDO boToDo(Long id, OAuth2BasicUserBO bo);

    /**
     * doToVo
     **/
    OAuth2BasicUserVO doToVo(OAuth2BasicUserDO entity);

    /**
     * dosToVos
     **/
    List<OAuth2BasicUserVO> dosToVos(List<OAuth2BasicUserDO> lists);

    /**
     * doToUserDetail
     **/
    OAuth2BasicUserDetails dosToUserDetail(OAuth2BasicUserDO entity);

}
