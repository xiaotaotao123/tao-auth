package cn.tao.auth.mapper;

import cn.tao.auth.dao.entity.OAuth2ThirdAccountDO;
import cn.tao.auth.service.bo.OAuth2ThirdAccountBO;
import cn.tao.auth.controller.vo.OAuth2ThirdAccountVO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * 三方登录账户信息表 mapper转换
 *
 * @author tao
 * @date 2024-08-19
 */
@Mapper(componentModel = "spring")
public interface OAuth2ThirdAccountMapper {

    /**
     * boToDo
     **/
    OAuth2ThirdAccountDO boToDo(OAuth2ThirdAccountBO bo);

    /**
     * boToDo
     **/
    OAuth2ThirdAccountDO boToDo(Long id, OAuth2ThirdAccountBO bo);

    /**
     * doToVo
     **/
    OAuth2ThirdAccountVO doToVo(OAuth2ThirdAccountDO entity);

    /**
     * dosToVos
     **/
    List<OAuth2ThirdAccountVO> dosToVos(List<OAuth2ThirdAccountDO> lists);

}
