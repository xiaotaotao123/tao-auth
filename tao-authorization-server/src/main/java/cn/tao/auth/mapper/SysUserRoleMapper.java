package cn.tao.auth.mapper;

import cn.tao.auth.dao.entity.SysUserRoleDO;
import cn.tao.auth.service.bo.SysUserRoleBO;
import cn.tao.auth.controller.vo.SysUserRoleVO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * 用户角色表 mapper转换
 *
 * @author tao
 * @date 2024-08-19
 */
@Mapper(componentModel = "spring")
public interface SysUserRoleMapper {

    /**
     * boToDo
     **/
    SysUserRoleDO boToDo(SysUserRoleBO bo);

    /**
     * boToDo
     **/
    SysUserRoleDO boToDo(Long id, SysUserRoleBO bo);

    /**
     * doToVo
     **/
    SysUserRoleVO doToVo(SysUserRoleDO entity);

    /**
     * dosToVos
     **/
    List<SysUserRoleVO> dosToVos(List<SysUserRoleDO> lists);

}
