package cn.tao.auth.mapper;

import cn.tao.auth.dao.entity.SysRoleDO;
import cn.tao.auth.service.bo.SysRoleBO;
import cn.tao.auth.controller.vo.SysRoleVO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * 角色 mapper转换
 *
 * @author tao
 * @date 2024-08-19
 */
@Mapper(componentModel = "spring")
public interface SysRoleMapper {

    /**
     * boToDo
     **/
    SysRoleDO boToDo(SysRoleBO bo);

    /**
     * boToDo
     **/
    SysRoleDO boToDo(Long id, SysRoleBO bo);

    /**
     * doToVo
     **/
    SysRoleVO doToVo(SysRoleDO entity);

    /**
     * dosToVos
     **/
    List<SysRoleVO> dosToVos(List<SysRoleDO> lists);

}
