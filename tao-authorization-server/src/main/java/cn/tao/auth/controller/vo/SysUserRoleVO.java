package cn.tao.auth.controller.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 用户角色表 VO
 *
 * @author tao
 * @date 2024-08-19
 */
@Data
@Schema(description = "用户角色表VO")
public class SysUserRoleVO {

    /**
     *
     */
    @Schema(description = "")
    private Integer id;

    /**
     * 角色ID
     */
    @Schema(description = "角色ID")
    private Integer roleId;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private Integer userId;

}
