package cn.tao.auth.controller;

/**
 * @Author tao
 * @Date 2024/8/20 22:49
 * @Description
 * @Version 1.0
 */

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import cn.tao.auth.controller.vo.CaptchaResultVO;
import cn.tao.result.ObjectResult;
import cn.tao.result.Result;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import jakarta.annotation.Resource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

import static cn.tao.auth.constant.RedisConstants.*;

/**
 * 登录接口，登录使用的接口
 *
 * @author vains
 */
@RestController
public class LoginController {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @GetMapping("/getSmsCaptcha")
    public ObjectResult<String> getSmsCaptcha(String phone) {
        // 示例项目，固定1234
        String smsCaptcha = "1234";
        // 存入缓存中，5分钟后过期
        redisTemplate.opsForValue().set((SMS_CAPTCHA_PREFIX_KEY + phone), smsCaptcha, CAPTCHA_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        return Result.success(smsCaptcha);
    }

    @GetMapping("/getCaptcha")
    public ObjectResult<CaptchaResultVO> getCaptcha() {
        // 使用huTool-captcha生成图形验证码
        // 定义图形验证码的长、宽、验证码字符数、干扰线宽度
        ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(150, 40, 4, 2);
        // 生成一个唯一id
        long id = IdWorker.getId();
        // 存入缓存中，5分钟后过期
        redisTemplate.opsForValue().set((IMAGE_CAPTCHA_PREFIX_KEY + id), captcha.getCode(), CAPTCHA_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        return Result.success(new CaptchaResultVO(String.valueOf(id), captcha.getCode(), captcha.getImageBase64Data()));
    }

}
