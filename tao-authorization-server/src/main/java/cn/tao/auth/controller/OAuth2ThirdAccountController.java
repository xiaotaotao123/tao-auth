package cn.tao.auth.controller;

import cn.tao.auth.controller.vo.OAuth2ThirdAccountVO;
import cn.tao.auth.controller.query.OAuth2ThirdAccountQuery;
import cn.tao.auth.service.bo.OAuth2ThirdAccountBO;
import cn.tao.auth.service.OAuth2ThirdAccountService;
import cn.tao.result.BaseResult;
import cn.tao.result.ObjectResult;
import cn.tao.result.PageResult;
import cn.tao.result.Result;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 三方登录账户信息表 Controller
 *
 * @author tao
 * @date 2024-08-19
 */
@Slf4j
@Validated
@RestController
@RequestMapping("oAuth2ThirdAccount")
public class OAuth2ThirdAccountController {

    @Resource
    private OAuth2ThirdAccountService oAuth2ThirdAccountService;

    @GetMapping("page")
    public PageResult<OAuth2ThirdAccountVO> getOAuth2ThirdAccountPage(OAuth2ThirdAccountQuery query) {
        return Result.success(oAuth2ThirdAccountService.findOAuth2ThirdAccountPage(query));
    }

    @GetMapping("list")
    public ObjectResult<List<OAuth2ThirdAccountVO>> getOAuth2ThirdAccountList(OAuth2ThirdAccountQuery query) {
        return Result.success(oAuth2ThirdAccountService.findOAuth2ThirdAccountList(query));
    }

    @PostMapping("add")
    public BaseResult addOAuth2ThirdAccount(@Valid @RequestBody OAuth2ThirdAccountBO oAuth2ThirdAccount) {
        this.oAuth2ThirdAccountService.addOAuth2ThirdAccount(oAuth2ThirdAccount);
        return Result.success("成功");
    }

    @PutMapping("update/{id}")
    public BaseResult updateOAuth2ThirdAccount(@PathVariable Long id, @Valid @RequestBody OAuth2ThirdAccountBO oAuth2ThirdAccount) {
        this.oAuth2ThirdAccountService.updateOAuth2ThirdAccount(id, oAuth2ThirdAccount);
        return Result.success("成功");
    }

    @DeleteMapping("delete/{id}")
    public BaseResult deleteOAuth2ThirdAccountById(@PathVariable Long id) {
        this.oAuth2ThirdAccountService.deleteOAuth2ThirdAccountById(id);
        return Result.success("成功");
    }

}
