package cn.tao.auth.controller.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 角色菜单 VO
 *
 * @author tao
 * @date 2024-08-19
 */
@Data
@Schema(description = "角色菜单VO")
public class SysRoleAuthorityVO {

    /**
     * 权限菜单ID
     */
    @Schema(description = "权限菜单ID")
    private Integer authorityId;

    /**
     * 角色菜单关联表自增ID
     */
    @Schema(description = "角色菜单关联表自增ID")
    private Integer id;

    /**
     * 角色ID
     */
    @Schema(description = "角色ID")
    private Integer roleId;

}
