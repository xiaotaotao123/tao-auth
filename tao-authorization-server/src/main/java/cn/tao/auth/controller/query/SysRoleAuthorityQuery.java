package cn.tao.auth.controller.query;

import cn.tao.base.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色菜单 query
 *
 * @author tao
 * @date 2024-08-19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "角色菜单query")
public class SysRoleAuthorityQuery extends BasePageQuery {

    /**
     * 权限菜单ID
     */
    @Schema(description = "权限菜单ID")
    private Integer authorityId;

    /**
     * 角色菜单关联表自增ID
     */
    @Schema(description = "角色菜单关联表自增ID")
    private Integer id;

    /**
     * 角色ID
     */
    @Schema(description = "角色ID")
    private Integer roleId;

}
