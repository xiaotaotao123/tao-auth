package cn.tao.auth.controller.query;

import cn.tao.base.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 三方登录账户信息表 query
 *
 * @author tao
 * @date 2024-08-19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "三方登录账户信息表query")
public class OAuth2ThirdAccountQuery extends BasePageQuery {

    /**
     * 博客地址
     */
    @Schema(description = "博客地址")
    private String blog;

    /**
     * 绑定时间
     */
    @Schema(description = "绑定时间")
    private Date createTime;

    /**
     * 自增id
     */
    @Schema(description = "自增id")
    private Integer id;

    /**
     * 地址
     */
    @Schema(description = "地址")
    private String location;

    /**
     * 三方登录类型
     */
    @Schema(description = "三方登录类型")
    private String type;

    /**
     * 三方登录唯一id
     */
    @Schema(description = "三方登录唯一id")
    private String uniqueId;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间")
    private Date updateTime;

    /**
     * 用户表主键
     */
    @Schema(description = "用户表主键")
    private Integer userId;

}
