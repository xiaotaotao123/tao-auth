package cn.tao.auth.controller;

import cn.tao.auth.controller.query.SysRoleAuthorityQuery;
import cn.tao.auth.controller.vo.SysRoleAuthorityVO;
import cn.tao.auth.service.SysRoleAuthorityService;
import cn.tao.auth.service.bo.SysRoleAuthorityBO;
import cn.tao.result.BaseResult;
import cn.tao.result.ObjectResult;
import cn.tao.result.PageResult;
import cn.tao.result.Result;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色菜单 Controller
 *
 * @author tao
 * @date 2024-08-19
 */
@Slf4j
@Validated
@RestController
@RequestMapping("sysRoleAuthority")
public class SysRoleAuthorityController {

    @Resource
    private SysRoleAuthorityService sysRoleAuthorityService;

    @GetMapping("page")
    public PageResult<SysRoleAuthorityVO> getSysRoleAuthorityPage(SysRoleAuthorityQuery query) {
        return Result.success(sysRoleAuthorityService.findSysRoleAuthorityPage(query));
    }

    @GetMapping("list")
    public ObjectResult<List<SysRoleAuthorityVO>> getSysRoleAuthorityList(SysRoleAuthorityQuery query) {
        return Result.success(sysRoleAuthorityService.findSysRoleAuthorityList(query));
    }

    @PostMapping("add")
    public BaseResult addSysRoleAuthority(@Valid @RequestBody SysRoleAuthorityBO sysRoleAuthority) {
        this.sysRoleAuthorityService.addSysRoleAuthority(sysRoleAuthority);
        return Result.success("成功");
    }

    @PutMapping("update/{id}")
    public BaseResult updateSysRoleAuthority(@PathVariable Long id, @Valid @RequestBody SysRoleAuthorityBO sysRoleAuthority) {
        this.sysRoleAuthorityService.updateSysRoleAuthority(id, sysRoleAuthority);
        return Result.success("成功");
    }

    @DeleteMapping("delete/{id}")
    public BaseResult deleteSysRoleAuthorityById(@PathVariable Long id) {
        this.sysRoleAuthorityService.deleteSysRoleAuthorityById(id);
        return Result.success("成功");
    }

}
