package cn.tao.auth.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author tao
 * @Date 2024/8/11 22:20
 * @Description
 * @Version 1.0
 */
@RestController
public class TestController {

    @GetMapping("/test1")
    @PreAuthorize("hasAuthority('SCOPE_message.read')")
    public String test01(){
        return "test01";
    }
}
