package cn.tao.auth.controller.query;

import cn.tao.base.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户角色表 query
 *
 * @author tao
 * @date 2024-08-19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "用户角色表query")
public class SysUserRoleQuery extends BasePageQuery {

    /**
     *
     */
    @Schema(description = "id")
    private Integer id;

    /**
     * 角色ID
     */
    @Schema(description = "角色ID")
    private Integer roleId;

    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private Integer userId;

}
