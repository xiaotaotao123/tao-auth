package cn.tao.auth.controller;

import cn.tao.auth.controller.query.SysRoleQuery;
import cn.tao.auth.controller.vo.SysRoleVO;
import cn.tao.auth.service.SysRoleService;
import cn.tao.auth.service.bo.SysRoleBO;
import cn.tao.result.BaseResult;
import cn.tao.result.ObjectResult;
import cn.tao.result.PageResult;
import cn.tao.result.Result;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色 Controller
 *
 * @author tao
 * @date 2024-08-19
 */
@Slf4j
@Validated
@RestController
@RequestMapping("sysRole")
public class SysRoleController {

    @Resource
    private SysRoleService sysRoleService;

    @GetMapping("page")
    public PageResult<SysRoleVO> getSysRolePage(SysRoleQuery query) {
        return Result.success(sysRoleService.findSysRolePage(query));
    }

    @GetMapping("list")
    public ObjectResult<List<SysRoleVO>> getSysRoleList(SysRoleQuery query) {
        return Result.success(sysRoleService.findSysRoleList(query));
    }

    @PostMapping("add")
    public BaseResult addSysRole(@Valid @RequestBody SysRoleBO sysRole) {
        this.sysRoleService.addSysRole(sysRole);
        return Result.success("成功");
    }

    @PutMapping("update/{id}")
    public BaseResult updateSysRole(@PathVariable Long id, @Valid @RequestBody SysRoleBO sysRole) {
        this.sysRoleService.updateSysRole(id, sysRole);
        return Result.success("成功");
    }

    @DeleteMapping("delete/{id}")
    public BaseResult deleteSysRoleById(@PathVariable Long id) {
        this.sysRoleService.deleteSysRoleById(id);
        return Result.success("成功");
    }

}
