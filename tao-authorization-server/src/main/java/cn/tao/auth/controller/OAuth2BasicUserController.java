package cn.tao.auth.controller;

import cn.tao.auth.controller.query.OAuth2BasicUserQuery;
import cn.tao.auth.controller.vo.OAuth2BasicUserVO;
import cn.tao.auth.service.OAuth2BasicUserService;
import cn.tao.auth.service.bo.OAuth2BasicUserBO;
import cn.tao.result.BaseResult;
import cn.tao.result.ObjectResult;
import cn.tao.result.PageResult;
import cn.tao.result.Result;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 基础用户信息表 Controller
 *
 * @author tao
 * @date 2024-08-19
 */
@Slf4j
@Validated
@RestController
@RequestMapping("oAuth2BasicUser")
public class OAuth2BasicUserController {

    @Resource
    private OAuth2BasicUserService oAuth2BasicUserService;

    @GetMapping("page")
    public PageResult<OAuth2BasicUserVO> getOAuth2BasicUserPage(OAuth2BasicUserQuery query) {
        return Result.success(oAuth2BasicUserService.findOAuth2BasicUserPage(query));
    }

    @GetMapping("list")
    public ObjectResult<List<OAuth2BasicUserVO>> getOAuth2BasicUserList(OAuth2BasicUserQuery query) {
        return Result.success(oAuth2BasicUserService.findOAuth2BasicUserList(query));
    }

    @PostMapping("add")
    public BaseResult addOAuth2BasicUser(@Valid @RequestBody OAuth2BasicUserBO oAuth2BasicUser) {
        this.oAuth2BasicUserService.addOAuth2BasicUser(oAuth2BasicUser);
        return Result.success("成功");
    }

    @PutMapping("update/{id}")
    public BaseResult updateOAuth2BasicUser(@PathVariable Long id, @Valid @RequestBody OAuth2BasicUserBO oAuth2BasicUser) {
        this.oAuth2BasicUserService.updateOAuth2BasicUser(id, oAuth2BasicUser);
        return Result.success("成功");
    }

    @DeleteMapping("delete/{id}")
    public BaseResult deleteOAuth2BasicUserById(@PathVariable Long id) {
        this.oAuth2BasicUserService.deleteOAuth2BasicUserById(id);
        return Result.success("成功");
    }

}
