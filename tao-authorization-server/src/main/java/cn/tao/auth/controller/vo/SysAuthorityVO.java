package cn.tao.auth.controller.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

/**
 * 系统菜单 VO
 *
 * @author tao
 * @date 2024-08-19
 */
@Data
@Schema(description = "系统菜单VO")
public class SysAuthorityVO {

    /**
     * 所需权限
     */
    @Schema(description = "所需权限")
    private String authority;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 创建人
     */
    @Schema(description = "创建人")
    private Integer createUserId;

    /**
     * 0:启用,1:删除
     */
    @Schema(description = "0:启用,1:删除")
    private Boolean deleted;

    /**
     * 菜单自增ID
     */
    @Schema(description = "菜单自增ID")
    private Integer id;

    /**
     * 父菜单ID
     */
    @Schema(description = "父菜单ID")
    private Integer menuPid;

    /**
     * 菜单名称
     */
    @Schema(description = "菜单名称")
    private String name;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Byte sort;

    /**
     * 0:菜单,1:接口
     */
    @Schema(description = "0:菜单,1:接口")
    private Byte type;

    /**
     * 跳转URL
     */
    @Schema(description = "跳转URL")
    private String url;

}
