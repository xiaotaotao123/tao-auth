package cn.tao.auth.controller.query;

import cn.tao.base.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 角色 query
 *
 * @author tao
 * @date 2024-08-19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "角色query")
public class SysRoleQuery extends BasePageQuery {

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 创建人
     */
    @Schema(description = "创建人")
    private Integer createUserId;

    /**
     * 0:启用,1:删除
     */
    @Schema(description = "0:启用,1:删除")
    private Boolean deleted;

    /**
     * 角色自增ID
     */
    @Schema(description = "角色自增ID")
    private Integer id;

    /**
     * 角色名
     */
    @Schema(description = "角色名")
    private String roleName;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer sort;

}
