package cn.tao.auth.controller;

import cn.tao.auth.controller.query.SysAuthorityQuery;
import cn.tao.auth.controller.vo.SysAuthorityVO;
import cn.tao.auth.service.SysAuthorityService;
import cn.tao.auth.service.bo.SysAuthorityBO;
import cn.tao.result.BaseResult;
import cn.tao.result.ObjectResult;
import cn.tao.result.PageResult;
import cn.tao.result.Result;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统菜单 Controller
 *
 * @author tao
 * @date 2024-08-19
 */
@Slf4j
@Validated
@RestController
@RequestMapping("sysAuthority")
public class SysAuthorityController {

    @Resource
    private SysAuthorityService sysAuthorityService;

    @GetMapping("page")
    public PageResult<SysAuthorityVO> getSysAuthorityPage(SysAuthorityQuery query) {
        return Result.success(sysAuthorityService.findSysAuthorityPage(query));
    }

    @GetMapping("list")
    public ObjectResult<List<SysAuthorityVO>> getSysAuthorityList(SysAuthorityQuery query) {
        return Result.success(sysAuthorityService.findSysAuthorityList(query));
    }

    @PostMapping("add")
    public BaseResult addSysAuthority(@Valid @RequestBody SysAuthorityBO sysAuthority) {
        this.sysAuthorityService.addSysAuthority(sysAuthority);
        return Result.success("成功");
    }

    @PutMapping("update/{id}")
    public BaseResult updateSysAuthority(@PathVariable Long id, @Valid @RequestBody SysAuthorityBO sysAuthority) {
        this.sysAuthorityService.updateSysAuthority(id, sysAuthority);
        return Result.success("成功");
    }

    @DeleteMapping("delete/{id}")
    public BaseResult deleteSysAuthorityById(@PathVariable Long id) {
        this.sysAuthorityService.deleteSysAuthorityById(id);
        return Result.success("成功");
    }

}
