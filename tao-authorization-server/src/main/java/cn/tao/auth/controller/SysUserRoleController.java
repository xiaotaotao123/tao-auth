package cn.tao.auth.controller;

import cn.tao.auth.controller.query.SysUserRoleQuery;
import cn.tao.auth.controller.vo.SysUserRoleVO;
import cn.tao.auth.service.SysUserRoleService;
import cn.tao.auth.service.bo.SysUserRoleBO;
import cn.tao.result.BaseResult;
import cn.tao.result.ObjectResult;
import cn.tao.result.PageResult;
import cn.tao.result.Result;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户角色表 Controller
 *
 * @author tao
 * @date 2024-08-19
 */
@Slf4j
@Validated
@RestController
@RequestMapping("sysUserRole")
public class SysUserRoleController {

    @Resource
    private SysUserRoleService sysUserRoleService;

    @GetMapping("page")
    public PageResult<SysUserRoleVO> getSysUserRolePage(SysUserRoleQuery query) {
        return Result.success(sysUserRoleService.findSysUserRolePage(query));
    }

    @GetMapping("list")
    public ObjectResult<List<SysUserRoleVO>> getSysUserRoleList(SysUserRoleQuery query) {
        return Result.success(sysUserRoleService.findSysUserRoleList(query));
    }

    @PostMapping("add")
    public BaseResult addSysUserRole(@Valid @RequestBody SysUserRoleBO sysUserRole) {
        this.sysUserRoleService.addSysUserRole(sysUserRole);
        return Result.success("成功");
    }

    @PutMapping("update/{id}")
    public BaseResult updateSysUserRole(@PathVariable Long id, @Valid @RequestBody SysUserRoleBO sysUserRole) {
        this.sysUserRoleService.updateSysUserRole(id, sysUserRole);
        return Result.success("成功");
    }

    @DeleteMapping("delete/{id}")
    public BaseResult deleteSysUserRoleById(@PathVariable Long id) {
        this.sysUserRoleService.deleteSysUserRoleById(id);
        return Result.success("成功");
    }

}
