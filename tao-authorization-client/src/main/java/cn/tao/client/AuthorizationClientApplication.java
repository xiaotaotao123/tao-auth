package cn.tao.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 * @author tao
 */
@SpringBootApplication
public class AuthorizationClientApplication {
    public static void main( String[] args ) {
        SpringApplication.run(AuthorizationClientApplication.class, args);
    }
}
