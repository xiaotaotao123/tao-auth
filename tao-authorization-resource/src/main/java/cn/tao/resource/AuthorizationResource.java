package cn.tao.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 * @author tao
 */
@SpringBootApplication
public class AuthorizationResource {
    public static void main( String[] args ) {
        SpringApplication.run(AuthorizationResource.class, args);
    }
}
